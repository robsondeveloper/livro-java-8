package br.com.casadocodigo.java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Capitulo02 {

	public static void main(String[] args) {
		Usuario usuario1 = new Usuario("Paulo Silveira", 150);
		Usuario usuario2 = new Usuario("Rodrigo Turini", 120);
		Usuario usuario3 = new Usuario("Guilherme Silveira", 190);

		List<Usuario> usuarios = Arrays.asList(usuario1, usuario2, usuario3);

		for (Usuario u : usuarios) {
			System.out.println(u.getNome());
		}

		usuarios.forEach(new Consumer<Usuario>() {
			public void accept(Usuario u) {
				System.out.println(u.getNome());
			}
		});

		usuarios.forEach(u -> System.out.println(u.getNome()));

		usuarios.forEach(u -> u.tornarModerador());
	}
}
