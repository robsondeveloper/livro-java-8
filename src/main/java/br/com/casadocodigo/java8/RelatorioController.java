package br.com.casadocodigo.java8;

import java.util.Arrays;

@Role("presidente")
@Role("diretor")
public class RelatorioController {

	public static void main(String[] args) {
		RelatorioController controller = new RelatorioController();
		Role[] annotationsByType = controller.getClass().getAnnotationsByType(Role.class);
		Arrays.asList(annotationsByType).forEach(a -> System.out.println(a.value()));
	}
}
