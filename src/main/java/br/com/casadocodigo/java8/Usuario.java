package br.com.casadocodigo.java8;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class Usuario {
	private String nome;
	private int pontos;
	private boolean moderador;

	public Usuario(String nome) {
		this.nome = nome;
	}

	public Usuario(String nome, int pontos) {
		this(nome);
		this.pontos = pontos;
	}

	public Usuario(String nome, int pontos, boolean moderador) {
		this(nome, pontos);
		this.moderador = moderador;
	}

	public String getNome() {
		return nome;
	}

	public int getPontos() {
		return pontos;
	}

	public void tornarModerador() {
		this.moderador = true;
	}

	public boolean isModerador() {
		return moderador;
	}

	@Override
	public String toString() {
		return "Usuario " + nome;
	}
}

class Grupo {
	private Set<Usuario> usuarios = new HashSet<>();

	public void add(Usuario u) {
		usuarios.add(u);
	}

	public Set<Usuario> getUsuarios() {
		return Collections.unmodifiableSet(this.usuarios);
	}
}
